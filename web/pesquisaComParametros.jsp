<%@page import="model.Usuario"%>
<%@page import="java.util.List"%>
<%@page import="dao.UsuarioDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="haeder.jsp"/>

<%
    String nome = request.getParameter("nome");
    //String nome = "";
    String apelido = request.getParameter("apelido");
    String id = request.getParameter("id");

    UsuarioDAO dao = new UsuarioDAO();
    List<Usuario> lista = dao.getListaComParametros(id, nome, apelido);
%>

<form>
    Id: <input type="text" name="id" /> 
    Nome: <input type="text" name="nome" /> 
    Apelido: <input type="text" name="apelido" /> 
    <input type="submit" value="Pesquisar" /><br/>
</form>

<br/>

<table border="1">
    <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Apelido</th>
        </tr>
    </thead>
    <tbody>

        <% for (Usuario u : lista) { %>

        <tr>
            <td><% out.print(u.getId()); %></td>
            <td><% out.print(u.getNome()); %></td>
            <td><% out.print(u.getApelido()); %></td>
        </tr>

        <% }%>

</table>




<jsp:include page="footer.jsp"/>
