package model;

public class Resposta {
    
    private int id;
    private String resposta;
    private String letra;
    private boolean correto;
    private int idPergunta;

    public Resposta() {
    }

    public Resposta(int id, String resposta, String letra, boolean correto, int idPergunta) {
        this.id = id;
        this.resposta = resposta;
        this.letra = letra;
        this.correto = correto;
        this.idPergunta = idPergunta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public boolean isCorreto() {
        return correto;
    }

    public void setCorreto(boolean correto) {
        this.correto = correto;
    }

    public int getIdPergunta() {
        return idPergunta;
    }

    public void setIdPergunta(int idPergunta) {
        this.idPergunta = idPergunta;
    }
    
    
    
    
    
    
}
