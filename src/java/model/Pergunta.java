package model;

import java.util.List;

public class Pergunta {
    
    private int id;
    private String pergunta;
    private List<Resposta> listaRespostas;

    public Pergunta() {
    }

    public Pergunta(int id, String pergunta) {
        this.id = id;
        this.pergunta = pergunta;
    }

    public List<Resposta> getListaRespostas() {
        return listaRespostas;
    }

    public void setListaRespostas(List<Resposta> listaRespostas) {
        this.listaRespostas = listaRespostas;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }
    
    
    
    
}
