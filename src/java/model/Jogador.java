package model;

public class Jogador {
    
    
    private int  id;
    private String nome;
    private String sobrenome;
    private int numeroJogo;

    public Jogador() {
    }

    public Jogador(int id, String nome, String sobrenome, int numeroJogo) {
        this.id = id;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.numeroJogo = numeroJogo;
    }

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenoma) {
        this.sobrenome = sobrenoma;
    }

    public int getNumeroJogo() {
        return numeroJogo;
    }

    public void setNumeroJogo(int numeroJogo) {
        this.numeroJogo = numeroJogo;
    }

    
    
    
    
}
