package servlet;

import dao.UsuarioDAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Usuario;

@WebServlet(urlPatterns = "/login.do")
public class LoginServlet extends HttpServlet{

    private UsuarioDAO dao ;
    private Usuario usuario;
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            
        String apelido = req.getParameter("usuario");
        String senha = req.getParameter("senha");
        
        dao = new UsuarioDAO();
        usuario = dao.login(apelido, senha);
        
        if (usuario == null ) {
            
            RequestDispatcher view = req.getRequestDispatcher("login.jsp");
            
            req.setAttribute("erro", "Usuário ou senha incorretos");
            
            view.forward(req, resp);
            
        }else{
        RequestDispatcher view = req.getRequestDispatcher("home.jsp");
       
        req.setAttribute("usuario", usuario);
        
        view.forward(req, resp);
        }
        
    } 
    
}
