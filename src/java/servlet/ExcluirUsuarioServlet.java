package servlet;

import dao.UsuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/excluirUsuario.do")
public class ExcluirUsuarioServlet extends HttpServlet {

    private UsuarioDAO dao = new UsuarioDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter saida = resp.getWriter();
        String nome = req.getParameter("nome");

        try {

            int id = Integer.parseInt(req.getParameter("id"));

            saida.println("<html>");
            saida.println("<body>");
            saida.println("O usuario " + nome + " foi EXCLUIDO <br/>");
            saida.println("<a href = './pesquisarUsuario.do'>Voltar</a>");
            saida.println("</body>");
            saida.println("</html>");

            dao.delete(id);
        } catch (Exception ex) {
            saida.println("<html>");
            saida.println("<body>");
            saida.println("Erro ao excluir "+nome +" <br/>");
            saida.println(ex.getClass() + " <br/>");
            saida.println("<a href = './pesquisarUsuario.do'>Voltar</a>");
            saida.println("</body>");
            saida.println("</html>");
        }

    }

}
