package servlet;

import dao.UsuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Usuario;


public class CadastrarUsuarioSarvlet extends HttpServlet {
    
    private Usuario usuario;
    private UsuarioDAO dao;
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        PrintWriter saida = resp.getWriter();
        
        try {
            
            String nome = req.getParameter("nome");
            String senha = req.getParameter("senha");
            String apelido = req.getParameter("apelido");
            
            usuario = new Usuario();
            
            usuario.setNome(nome);
            usuario.setApelido(apelido);
            usuario.setSenha(senha);
            
            dao = new UsuarioDAO();
            dao.inserir(usuario);
            
            saida.println("<html>");
            saida.println("<body>");
            saida.println("Salvo com sucesso!");
            saida.println("</body>");
            saida.println("</html>");
            
        } catch (Exception ex) {
             saida.println("<html>");
            saida.println("<body>");
            saida.println("Erro ao Salvar <br/> " + ex.getClass());
            saida.println("</body>");
            saida.println("</html>");
        }
    }
}
