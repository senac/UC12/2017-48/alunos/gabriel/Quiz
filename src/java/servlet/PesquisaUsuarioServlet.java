package servlet;

import dao.UsuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Usuario;

@WebServlet(urlPatterns = "/pesquisarUsuario.do")
public class PesquisaUsuarioServlet  extends HttpServlet{

    private UsuarioDAO dao = new UsuarioDAO();
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        PrintWriter saida = resp.getWriter();
        
        try {
        List<Usuario> lista = dao.getLista();
            
        
            saida.println("<html>");
            saida.println("<body>");
            
            saida.println("<table border='1'>");
            
            saida.println("<tr><td>ID</td><td>NOME</td><td>APELIDO</td><td></td></tr>");
            
            for(Usuario u : lista){
             saida.println("<tr><td>"+u.getId()+"</td><td>"+u.getNome()+"</td><td>"+u.getApelido()+"</td><td> <a href ='./excluirUsuario.do?id="+u.getId()+"&nome="+u.getNome()+" '> Excluir </a></td></tr>");
            }
            
            saida.println("</table>");
            
            saida.println("</body>");
            saida.println("</html>");
        } catch (Exception ex) {
            saida.println("<html>");
            saida.println("<body>");
            saida.println("Erro ao Pesquisar <br/> " + ex.getClass());
            saida.println("</body>");
            saida.println("</html>");
        }
        
    }
    
    
    
}
