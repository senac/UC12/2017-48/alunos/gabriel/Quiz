package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Usuario;

public class UsuarioDAO implements DAO<Usuario> {

    @Override
    public List<Usuario> getLista() {
        List<Usuario> lista = new ArrayList<>();
        Connection connection = Conexao.getConnection();

        try {
            String query = "select * from usuario ;";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                String senha = rs.getString("senha");
                String apelido = rs.getString("apelido");

                Usuario usuario = new Usuario(id, nome, senha, apelido);
                lista.add(usuario);
            }
            System.out.println("Sucesso ao executar a query ....");
        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }

        return lista;

    }

    @Override
    public void inserir(Usuario objeto) {
        Connection connection = Conexao.getConnection();

        Usuario usuario = objeto;

        try {
            String query = "insert into usuario (nome, senha, apelido) value (' " + usuario.getNome() + " ', ' " + usuario.getSenha() + " ',' " + usuario.getApelido() + " ');";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);

            System.out.println("Sucesso ao executar a query ....");

        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao desconectar....");
            }
        }

    }

    @Override
    public void atualizar(Usuario objeto) {
    }

    @Override
    public void delete(int id) {

        Connection connection = Conexao.getConnection();

        try {
            String query = "delete from usuario where id = " + id;
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);

            System.out.println("Sucesso ao excluir a query ....");

        } catch (SQLException ex) {
            System.out.println("Erro excluir a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao desconectar....");
            }
        }

    }

    @Override
    public Usuario getPorId(int id) {
        return null;
    }

    public List<Usuario> getListaComParametros(String id, String nome, String apelido) {
        List<Usuario> lista = new ArrayList<>();
        Connection connection = Conexao.getConnection();

        try {
            String query = "select * from usuario where 1 = 1 and id like ? and nome like ? and apelido like ? ;";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, id + "%");
            ps.setString(2, nome + "%");
            ps.setString(3, apelido + "%");

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                Usuario usuario = new Usuario(rs.getInt("id"), rs.getString("nome"), rs.getString("senha"), rs.getString("apelido"));
                lista.add(usuario);
            }
            System.out.println("Sucesso ao executar a query ....");
        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }

        return lista;

    }

    public Usuario login(String apelido, String senha) {
       
        Usuario usuario = null;
        
        Connection connection = Conexao.getConnection();
        
         try {
            String query = "select * from usuario where apelido = ? and senha = ?;";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, apelido );
            ps.setString(2, senha );

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                usuario = new Usuario(rs.getInt("id"), rs.getString("nome"), rs.getString("senha"), rs.getString("apelido"));
                
            }
            System.out.println("Sucesso ao executar a query ....");
        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }
         
        return usuario;
    }

}
