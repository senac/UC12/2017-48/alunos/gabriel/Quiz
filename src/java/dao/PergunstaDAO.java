package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import model.Pergunta;
import model.Resposta;

public class PergunstaDAO implements DAO<Pergunta> {

    @Override
    public List<Pergunta> getLista() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void inserir(Pergunta pergunta) {

        Connection connection = Conexao.getConnection();

        try {
            connection.setAutoCommit(false);

            String queryInsertPergunta = "INSERT INTO pergunta (Pergunta) VALUES (?);";

            PreparedStatement ps = connection.prepareStatement(queryInsertPergunta, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, pergunta.getPergunta());
            System.out.println(ps);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.first();
            int idPergunta = rs.getInt(1);
            pergunta.setId(idPergunta);

            String queryResposta = "INSERT INTO resposta (Resposta, Letra, Correta, idPergunta) VALUES (?,?,?,?);";
            ps = connection.
                    prepareStatement(queryResposta, PreparedStatement.RETURN_GENERATED_KEYS);

            for (Resposta r : pergunta.getListaRespostas()) {
                ps.setString(1, r.getResposta());
                ps.setString(2, r.getLetra());
                ps.setBoolean(3, r.isCorreto());
                ps.setInt(4, idPergunta);

                ps.executeUpdate();
                rs = ps.getGeneratedKeys();
                rs.first();
                int idResposta = rs.getInt(1);
                r.setId(idResposta);

            }

            connection.commit();

        } catch (Exception ex) {

            try {
                connection.rollback();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }

            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    @Override
    public void atualizar(Pergunta objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pergunta getPorId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
