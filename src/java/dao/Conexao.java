package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Usuario;


public class Conexao {
    
    private static final String URL = "jdbc:mysql://localhost:3306/Quiz"; 
    private static final String  USER = "root";
    private static final String  PASSWORD = "123456";
    private static final String  DRIVE = "com.mysql.jdbc.Driver";
    
    public static Connection getConnection(){
        Connection connection = null;
        
        try {
            Class.forName(DRIVE);
            
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            
        } catch (ClassNotFoundException ex) {
            System.out.println("Erro carregar o drive...");
        } catch (SQLException ex) {
            System.out.println("Erro ao conectar no banco...");
        }
        
        return connection;
    }
    
    public static void select(){
        
        try {
            Connection connection = Conexao.getConnection();
        
            String query = "select * from usuario;";
        
            Statement statement;
            statement = connection.createStatement();
        
            ResultSet rs = statement.executeQuery(query);
        
            List<Usuario> lista = new ArrayList<>();
            
            while(rs.next()){
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                String senha = rs.getString("senha");
                String apelido = rs.getString("apelido");
                
                Usuario usuario = new Usuario(id, nome, senha, apelido);
                
                lista.add(usuario);
                
            }
        
        connection.close();
        
        } catch (SQLException ex) {
            System.out.println("Erro ao executar query...");
        }
        
        
    }
    
}
