package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Jogador;
import model.Usuario;

public class JogadorDAO implements DAO<Jogador> {

    @Override
    public List<Jogador> getLista() {
        List<Jogador> lista = new ArrayList<>();
        Connection connection = Conexao.getConnection();

        try {
            String query = "select * from jogador ;";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                String sobrenome = rs.getString("sobrenome");
                int numeroJogo = rs.getInt("numeroJogo");

                Jogador jogador= new Jogador(id, nome, sobrenome, numeroJogo);
                lista.add(jogador);
            }
            System.out.println("Sucesso ao executar a query ....");
        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }

        return lista;
    }

    @Override
    public void inserir(Jogador objeto) {
        Connection connection = Conexao.getConnection();

        Jogador jogador = objeto;

        try {
            String query = "insert into jogador (nome, sobrenome, numeroJogo) value (?, ?, ?);";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, jogador.getNome());
            ps.setString(2, jogador.getSobrenome());
            ps.setInt(3, jogador.getNumeroJogo());
            
            ps.executeUpdate();

            System.out.println(ps);
            System.out.println("Sucesso ao executar a query ....");

        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao desconectar....");
            }
        }
    }

    @Override
    public void atualizar(Jogador objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        Connection connection = Conexao.getConnection();

        try {
            String query = "delete from jogador where id = ?;";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            
            ps.executeUpdate();

            System.out.println("Sucesso ao excluir a query ....");

        } catch (SQLException ex) {
            System.out.println("Erro excluir a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("Erro ao desconectar....");
            }
        }
    }

    @Override
    public Jogador getPorId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
